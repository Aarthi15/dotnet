﻿using Microsoft.AspNetCore.Mvc;
using TodoAPI.Models.Domain;
using TodoAPI.Models.DTO;

namespace TodoAPI.repository
{
    public interface ITodoRepository
    {
        List<Todo> GetAll(string? filterOn=null,string? filterQuery=null,int pageNumber=1,int pageSize=1000);
        List<Todo> Create();
    }
}
