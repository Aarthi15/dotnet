﻿using Microsoft.EntityFrameworkCore;
using TodoAPI.Data;
using TodoAPI.Models.Domain;
using TodoAPI.Models.DTO;

namespace TodoAPI.repository
{
    public class SQLTodoRepository : ITodoRepository
    {
        private TodoDbContext dbContext;
        public SQLTodoRepository(TodoDbContext dbContext) 
        {
          this.dbContext = dbContext;
        }

        public List<Todo> GetAll(string? filterOn=null,string? filterQuery=null,int pageNumber=1,int pageSize=1000)
        {  
            var Todo = dbContext.Todo.AsQueryable();
            foreach (var todos in Todo)
            {
                DateTime d = DateTime.Now;
                if (todos.Duedate < d)
                {
                    todos.Status = "Completed";
                }
                else
                {
                    todos.Status = "Pending";
                }
            }
            dbContext.SaveChanges();

            if(string.IsNullOrWhiteSpace(filterOn)==false && string.IsNullOrWhiteSpace(filterQuery)==false)
            {
                if(filterOn.Equals("Status",StringComparison.OrdinalIgnoreCase)) 
                {
                 Todo=Todo.Where(x=>x.Status.Contains(filterQuery));
                }
            }

            var skipResult = (pageNumber - 1) * pageSize;
           return Todo.Skip(skipResult).Take(pageSize).ToList();
           
        }

        public List<Todo> Create()
        {
            return dbContext.Todo.ToList();
        }
    }
}
