﻿using Microsoft.EntityFrameworkCore;
using TodoAPI.Models.Domain;

namespace TodoAPI.Data
{
    public class TodoDbContext:DbContext
    {
        public TodoDbContext(DbContextOptions<TodoDbContext> options) : base(options) 
        {
        
        }

        public DbSet<Todo> Todo {  get; set; }
    }
}
