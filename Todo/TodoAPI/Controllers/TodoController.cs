﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TodoAPI.Data;
using TodoAPI.Mapping;
using TodoAPI.Models.Domain;
using TodoAPI.Models.DTO;
using TodoAPI.repository;

namespace TodoAPI.Controllers
{
    [ApiController]
    [Route("api/[Controller]")]
    public class TodoController : Controller
    {
        private TodoDbContext dbContext;
        private ITodoRepository todoRepository;
        private IMapper mapper;
       public TodoController(TodoDbContext dbContext, ITodoRepository todoRepository, IMapper mapper)
        { 
            this.dbContext = dbContext;
            this.todoRepository = todoRepository;
            this.mapper = mapper;
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery]string? filterOn,[FromQuery] string? filterQuery,[FromQuery] int pageNumber = 1, [FromQuery] int pageSize=1000)
        {
            
            var Todo = todoRepository.GetAll(filterOn,filterQuery,pageNumber,pageSize);
            
           return Ok(Todo);
        }

        [HttpPost]
        public IActionResult Create([FromBody] TodoRequestDto todoRequestDto)
        {
            var TodoDomain = mapper.Map<Todo>(todoRequestDto);

            dbContext.Todo.Add(TodoDomain);
            dbContext.SaveChanges();

            var TodoDto = mapper.Map<TodoRequestDto>(TodoDomain);

            return CreatedAtAction(nameof(GetById), new { id = TodoDomain.Id }, TodoDomain);
        }


        [HttpGet]
        [Route("{id:int}")]
        public IActionResult GetById([FromRoute] int id)
        {
            var Todo = dbContext.Todo.FirstOrDefault(x => x.Id == id);
            if (Todo == null)
            {
                return NotFound();
            }
            return Ok(Todo);
        }
    }
}
