﻿namespace TodoAPI.Models.DTO
{
    public class TodoRequestDto
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Duedate { get; set; }
        
    }
}
