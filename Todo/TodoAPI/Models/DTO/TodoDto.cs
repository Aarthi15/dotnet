﻿namespace TodoAPI.Models.DTO
{
    public class TodoDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Duedate { get; set; }
        public String Status { get; set; }
    }
}
