﻿namespace TodoAPI.Models.Domain
{
    public class Todo
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Duedate { get; set;}
        public string? Status { get; set; }

    }
}
