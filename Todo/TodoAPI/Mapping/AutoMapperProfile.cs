﻿using AutoMapper;
using TodoAPI.Models.Domain;
using TodoAPI.Models.DTO;

namespace TodoAPI.Mapping
{
    public class AutoMapperProfile:Profile
    {

        public AutoMapperProfile() 
        {
           CreateMap<Todo,TodoDto>().ReverseMap();
            CreateMap<TodoRequestDto,Todo>().ReverseMap();
        }
    }
}
